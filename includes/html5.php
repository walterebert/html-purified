<?php
/**
 * W3C HTML 5 + HTML 5.1 definitions
 *
 * Based on:
 * https://www.w3.org/wiki/HTML/New_HTML5_Elements
 * https://www.w3.org/TR/html5/
 * https://www.w3.org/html/wg/drafts/html/master/single-page.html
 *
 * HTML Purifier has no type for floats and 'Text' is used instead
 * http://htmlpurifier.org/docs/enduser-customize.html
 *
 * TODO: SVG, MathML, ARIA + data attributes
 *
 * @author  Walter Ebert (http://walterebert.com)
 * @package HTML Purified
 */

$config->set( 'HTML.Doctype', 'HTML 4.01 Transitional' );
$config->set( 'HTML.CustomDoctype', 'HTML 5' );
$config->set( 'HTML.DefinitionID', 'html-5' );
$config->set( 'HTML.DefinitionRev', 1 );

if ( $def = $config->maybeGetRawHTMLDefinition() ) {
	$def->addElement( 'address', 'Block', 'Flow', 'Common' );

	$def->addElement( 'article', 'Block', 'Flow', 'Common' );

	$def->addElement( 'aside', 'Block', 'Flow', 'Common' );

	$def->addElement( 'audio', 'Block', 'Optional: (source, Flow) | (Flow, source) | Flow', 'Common', array(
		'src' => 'URI',
		'crossorigin' => 'Text',
		'preload' => 'Enum#auto,metadata,none',
		'autoplay' => 'Bool',
		'mediagroup' => 'Text',
		'loop' => 'Bool',
		'muted' => 'Bool',
		'type' => 'Text',
		'controls' => 'Bool',
	) );

	$def->addElement( 'canvas', 'Block', 'Flow', 'Common', array(
		'width' => 'Text',
		'label' => 'Text',
	) );

	$def->addElement( 'command', 'Block', 'Flow', 'Common', array(
		'type' => 'Enum#command,checkbox,radio',
		'label' => 'Text',
		'icon' => 'URI',
		'disabled' => 'Bool',
		'checked' => 'Bool',
		'radiogroup' => 'Text',
	) );

	$def->addElement( 'datalist', 'Block', 'Flow', 'Common' );

	$def->addElement( 'details', 'Block', 'Flow', 'Common', array(
		'open' => 'Bool',
	) );

	$def->addAttribute( 'del', 'cite', 'Text' );
	$def->addAttribute( 'del', 'datetime', 'Text' );
	
	$def->addElement( 'embed', 'Block', 'Flow', 'Common', array(
		'src' => 'URI',
		'type' => 'Text',
		'width' => 'Length',
		'height' => 'Length',
	) );

	$def->addElement( 'figure', 'Block', 'Optional: (figcaption, Flow) | (Flow, figcaption) | Flow', 'Common' );

	$def->addElement( 'figcaption', 'Inline', 'Flow', 'Common' );

	$def->addElement( 'footer', 'Block', 'Flow', 'Common' );

	$def->addElement( 'header', 'Block', 'Flow', 'Common' );

	$def->addElement( 'hgroup', 'Block', 'Required: h1 | h2 | h3 | h4 | h5 | h6', 'Common' );

	$def->addAttribute( 'img', 'srcset', 'Text' );
	$def->addAttribute( 'img', 'sizes', 'Text' );

	$def->addAttribute( 'input', 'autofocus', 'Bool' );
	$def->addAttribute( 'input', 'autocomplete', 'Bool' );
	$def->addAttribute( 'input', 'dirname', 'Text' );
	$def->addAttribute( 'input', 'list', 'Text' );
	$def->addAttribute( 'input', 'minlength', 'Length' );
	$def->addAttribute( 'input', 'multiple', 'Bool' );
	$def->addAttribute( 'input', 'placeholder', 'Text' );
	$def->addAttribute( 'input', 'required', 'Bool' );
	$def->addAttribute( 'input', 'step', 'Text' );

	$def->addAttribute( 'ins', 'cite', 'Text' );
	$def->addAttribute( 'ins', 'datetime', 'Text' );

	$def->addElement( 'keygen', 'Block', 'Flow', 'Common', array(
		'autofocus' => 'Bool',
		'challenge' => 'Text',
		'disabled' => 'Bool',
		'form' => 'ID',
		'keytype' => 'Text',
		'name' => 'Text',
		'altimg-valign' => 'Text',
		'alttext' => 'Text',
		'cdgroup' => 'URI',
	) );

	$def->addElement( 'main', 'Block', 'Flow', 'Common' );

	$def->addElement( 'mark', 'Block', 'Flow', 'Common' );

	$def->addElement( 'meter', 'Block', 'Flow', 'Common', array(
		'value' => 'Text',
		'min' => 'Text',
		'max' => 'Text',
		'low' => 'Text',
		'high' => 'Text',
		'optimum' => 'Text',
		'form' => 'ID',
	) );

	$def->addElement( 'nav', 'Block', 'Flow', 'Common' );

	$def->addElement( 'output', 'Block', 'Flow', 'Common', array(
		'for' => 'NMTOKENS',
		'form' => 'ID',
		'name' => 'Text',
	) );

	$def->addElement( 'picture', 'Block', 'Optional: (source, Flow) | (Flow, source) | Flow', 'Common', array(
		'srcset' => 'Text',
		'sizes' => 'Text',
		'media' => 'Text',
		'type' => 'Text',
	) );

	$def->addElement( 'progress', 'Block', 'Flow', 'Common', array(
		'value' => 'Text',
		'max' => 'Text',
		'form' => 'ID',
	) );

	$def->addElement( 'rp', 'Block', 'Flow', 'Common' );

	$def->addElement( 'rt', 'Block', 'Flow', 'Common' );

	$def->addElement( 'ruby', 'Block', 'Flow', 'Common' );

	$def->addElement( 'section', 'Block', 'Flow', 'Common' );

	$def->addElement( 'source', 'Block', 'Flow', 'Common', array(
		'src' => 'URI',
		'type' => 'Text',
		'srcset' => 'Text', // When used with picture.
		'sizes' => 'Text', // When used with picture.
	) );

	$def->addElement( 'summary', 'Block', 'Flow', 'Common' );

	$def->addElement( 'template', 'Block', 'Flow', 'Common' );

	$def->addElement( 'time', 'Inline', 'Flow', 'Common', array(
		'datetime' => 'Text',
		'pubdate' => 'Text',
	) );

	$def->addElement( 'track', 'Block', 'Flow', 'Common', array(
		'kind' => 'Text',
		'src' => 'URI',
		'srclang' => 'Text',
		'label' => 'Text',
		'default' => 'Text',
	) );

	$def->addElement( 'video', 'Block', 'Optional: (source, Flow) | (Flow, source) | Flow', 'Common', array(
		'autoplay' => 'Bool',
		'controls' => 'Bool',
		'height' => 'Length',
		'loop' => 'Bool',
		'poster' => 'URI',
		'preload' => 'Enum#auto,metadata,none',
		'mediagroup' => 'Text',
		'muted' => 'Bool',
		'src' => 'URI',
		'type' => 'Text',
		'width' => 'Length',
	) );

	$def->addElement( 'wbr', 'Inline', 'Flow', 'Common' );
}
