��    '      T  5   �      `  1   a     �     �     �  @   �  1        I     ]  `   k     �     �     �                #     1     ?  	   U     _     }     �     �     �     �     �     �     �     �  (   �       5   (  ,   ^  �  �     P     ^     o  	   �     �  �  �  7   ^
  3   �
      �
     �
  G   �
  B   A     �     �  x   �          6     I     Z     q     }     �     �  	   �  !   �     �     �     �     �     �            	   ,     6  0   Q  2   �  <   �  "   �  �       �     �     �  	     %            '                
   "          	          $                         %                                 #              &                                       !                  <div class="wrap"><h2>Processing %d comments</h2> Add message to site footer Additionally run HTML Tidy Allowed tags Also pass data for admin users through HTML filter.  Be careful! Any URL matching a pattern be removed from input. Cache HTML Purifier Document Type Enter <strong><code>wordpress</code></strong> to reset allowed tags to WordPress default.</span> Filter admin users General Options HTML 4.01 Strict HTML 4.01 Transitional HTML Filter HTML Purified HTML Purifier HTML Purifier Options HTML Tidy HTML tags allowed in comments Heavy KSES Light Medium None Purifier Options Purify Comments Save Separate tags with a new line. Specify each pattern on a separate line. Tag attributes can specified as The cache directory <code>%s</code> is not writeable. This should match the DOCTYPE for your theme This site is protected with <a style="color: #ddd; background-color: #333" href="http://urbangiraffe.com/">Urban Giraffe's</a> plugin '<a style="color: #ddd; background-color: #333" href="http://urbangiraffe.com/plugins/html-purified/">HTML Purified</a>' and Edward Z. Yang's <a href="http://htmlpurifier.org/"><img src="http://htmlpurifier.org/live/art/powered.png" align="middle" alt="Powered by HTML Purifier" border="0" width="80" height="15"/></a> URL Blacklist XHTML 1.0 Strict XHTML 1.0 Transitional XHTML 1.1 Your options have been saved Project-Id-Version: HTML Purified
POT-Creation-Date: 
PO-Revision-Date: 2009-08-31 22:27+0200
Last-Translator: Satyr <andreas@beraz.de>
Language-Team: Beraz 
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Germany
X-Poedit-Country: Germany
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: .
X-Poedit-SourceCharset: utf-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
 <div class="wrap"><h2>In Bearbeitung %d Kommentare</h2> Fügen Sie bitte die Meldung in Seite-Footer hinzu. Zusätzlich HTML Tidy ausführen Erlaubte Tags Die Daten haben für Admin-Benuzer durch HTML Filter gepasst. Pass auf! Jede URL, die dem Muster entspricht, wird beim Speichern entfernt. Cachen Sie HTML Purifier Dateityp Geben Sie <strong><code>wordpress</code></strong> ein, um die erlaubte Tags zu Wordpress-Default zurückzusetzen.</span> Filtern Admin Benutzer Haupteinstellungen HTML 4.01 Strict HTML 4.01 Transitional HTML-Filter HTML Purified HTML Purifier Einstellungen von HTML Purifier HTML Tidy Erlaubte in Kommentaren HTML-Tags Groß KSES Leicht Mittel Nein Purifier Einstellungen Purify Kommentare Speichern Trenne Tags durch [ENTER]. Geben Sie jedes Muster in einer neuen Zeile ein. Die Tags-Atributte können so spezifiziert werden: Das Cache-Verzeichnis <code>%s</code> ist schreibgeschützt. DOCTYPE muss passen zu Ihrem Thema Die Seite ist  geschützt durch <a style="color: #ddd; background-color: #333" href="http://urbangiraffe.com/">Urban Giraffe's</a> Plugin '<a style="color: #ddd; background-color: #333" href="http://urbangiraffe.com/plugins/html-purified/">HTML Purified</a>' и Edward Z. Yang's <a href="http://htmlpurifier.org/"><img src="http://htmlpurifier.org/live/art/powered.png" align="middle" alt="Powered by HTML Purifier" border="0" width="80" height="15"/></a> URL Blacklist XHTML 1.0 Strict XHTML 1.0 Transitional XHTML 1.1 Ihre Einstellungen wurden gespeichert 